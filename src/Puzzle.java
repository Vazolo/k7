import java.util.*;

public class Puzzle {

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {

      String[][] ar = {args[0].split(""), args[1].split(""), args[2].split("")};

      LinkedHashSet<String> uniqueLetters = new LinkedHashSet<>();
      for (String arg : args) {
         uniqueLetters.add(arg.split("")[0]);
      }
      int firstLettersCount = uniqueLetters.size(); // !!
      for (String arg : args) {
         uniqueLetters.addAll(Arrays.asList(arg.split("")));
      }

      HashMap<String, Integer> letterMap = new HashMap<>(); // !! (letter, its position at unique letters)
      List<Integer> number = new ArrayList<>(uniqueLetters.size()); // !!
      int i = 0;
      for (String ul : uniqueLetters) {
         number.add(0);
         letterMap.put(ul, i);
         i++;
      }
      int count = checkAllCombinations(number, 0, firstLettersCount, letterMap, ar);
      System.out.println(count);
   }

   private static int checkAllCombinations
           (List<Integer> number, int subListIndex, int flc, HashMap<String, Integer> lp, String[][] ar){
      Stack<Integer> availableNums = new Stack<>();

      for (int i = 0; i < 10; i++) {
         if (!number.subList(0,subListIndex).contains(i)){
            availableNums.push(i);
         }
      }

      int counter = 0;
      boolean isLastEl = subListIndex == number.size() - 1;
      int iterations = availableNums.size();
      for (int i = 0; i < iterations; i++) {
         Integer curNum = availableNums.pop();

         if (curNum == 0 && subListIndex <= flc - 1){ // 1. constraint
            continue;
         }
         number.set(subListIndex, curNum
         );
         if (!isLastEl){
            counter += checkAllCombinations(number, subListIndex + 1, flc, lp, ar);
         }
         else {
            if(checkTheEquation(number, lp, ar)){
               counter++;
            }
         }
      }
      return counter;
   }

   private static boolean checkTheEquation(List<Integer> number, HashMap<String, Integer> lp, String[][] args){
      List<Long> terms = Arrays.asList(0L, 0L, 0L);
      int termIndex = 0;
      int numberIndex = 0;
      for (String[] ar : args) {
         for (int i = ar.length - 1; i >= 0; i--) {
            String s = ar[i];
            terms.set(termIndex, (long) (terms.get(termIndex) + number.get(lp.get(s)) * Math.pow(10, numberIndex)));
            numberIndex++;
         }
         numberIndex = 0;
         termIndex++;
      }
      return terms.get(0) + terms.get(1) == terms.get(2);
   }

}

